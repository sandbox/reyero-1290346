<?php
/**
 * @file
 *   String module, string hooks.
 */


/**
 * Implements hook_string_type_info()
 */
function string_string_type_info() {
  $types['plain'] = array(
    'title' => t('Plain text'),
    'translation class' => 'String_Simple',
    'description' => t('Plain text, filtered with check_plain().'),
  );
  $types['text'] = array(
    'title' => t('Text'),
    'translation class' => 'String_Text',
    'description' => t('Multiline text with text format.'),
  );
  $format['filter'] = array(
    'title' => t('Filter XSS'),
    'translation class' => 'String_Filter',
    'description' => t('Filter out not explicitly allowed HTML tags.'),
    'allowed_tags' => array('a', 'em', 'strong', 'cite', 'blockquote', 'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd'),
  );
  $format['admin'] = array(
    'title' => t('Filter XSS Admin'),
    'translation class' => 'String_Filter',
    'description' => t('Filter out not explicitly allowed HTML tags.'),
    'allowed_tags' => array('a', 'abbr', 'acronym', 'address', 'article', 'aside', 'b', 'bdi', 'bdo', 'big', 'blockquote', 'br', 'caption', 'cite', 'code', 'col', 'colgroup', 'command', 'dd', 'del', 'details', 'dfn', 'div', 'dl', 'dt', 'em', 'figcaption', 'figure', 'footer', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'header', 'hgroup', 'hr', 'i', 'img', 'ins', 'kbd', 'li', 'mark', 'menu', 'meter', 'nav', 'ol', 'output', 'p', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'section', 'small', 'span', 'strong', 'sub', 'summary', 'sup', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead', 'time', 'tr', 'tt', 'u', 'ul', 'var', 'wbr'),
  );
  return $types;
}


