<?php
/**
 * @param
 * String module classes
 */

/**
 * Interface for all string objects.
 */
interface String_Interface {
  /**
   * Get and format string applying format and filter.
   */
  public function format($options = array());
  /**
   * Format as html
   */
  public function format_html($options = array());
  /**
   * Format as plain text
   */
  public function format_plain($options = array());
  /**
   * Get string name.
   */
  public function get_name();
  /**
   * Get string path for administration.
   */
  public function get_path();
 /**
   * Get string title from module defaults.
   */
  public function get_title();
  /**
   * Form element for viewing / editing string.
   */
  public function form_element();
}

/**
 * Base class for all string objects
 */
abstract class String_Object implements String_Interface {
  /**
   * Standard constructor
   */
  public function __construct($data = NULL) {
    if ($data) {
      foreach ((array)$data as $field => $value) {
        if (property_exists($this, $field)) {
          $this->$field = $value;
        }
      }
    }
  }
  /**
   * Default string format will be 'plain'
   */
  public function __toString() {
    return $this->format_plain();
  }
  /**
   * Get string path for administration.
   */
  function get_path() {
    return STRING_MENU_PATH . '/string/' . $this->get_name();
  }
  /**
   * Invoke hook_string_OPERATION on all modules.
   */
  protected function module_invoke($op) {
    module_invoke_all('string_' . $op, $this);
    return $this;
  }
}

/**
 * String abstraction that contains metadata and multiple translations
 */
class String extends String_Object {
  // String unique id
  public $sid = NULL;
  // String machine name, usually 'module.type.index.property'
  public $name;
  // String type
  public $type = 'string';
  // Parent string for nested strings.
  public $parent = '';
  // Item key for string lists
  public $itemkey = '';
  // Name parts for better indexing. The first part will be module name.
  public $module;
  // Weight for list values
  public $weight = 0;
  // Whether this string is enabled
  public $status = 1;
  // Whether this string needs token replacement
  public $token = 0;
  // String translations, where the actual texts live
  protected $translation;
  // String default when building strings on the fly
  protected $default;
  // Administrative properties, not saved.
  public $title;
  public $description;

  /**
   * Standard constructor
   */
  public function __construct($data = NULL) {
    parent::__construct($data);
    // Set default as proper string text object.
    if (isset($this->default)) {
      $this->default = $this->build_string($this->default);
    }
    // Set translations as proper string text objects.
    if (isset($this->translation)) {
      foreach ($this->translation as $langcode => $translation) {
        $this->set_translation($langcode, $translation);
      }
    }
  }
  /**
   * Build string text of the right type
   *
   * @param $langcode
   *  Language for this string.
   * @param $data
   *   Plain string or array data
   */
  protected function build_string($data) {
    $data = is_string($data) ? array('value' => $data) : $data;
    $data += array('language' => language_default('language'));
    $class = $this->get_info('class', 'String_Translation');
    return new $class($data, $this);
  }
  /**
   * Get and format string applying format and filter.
   */
  function format($options = array()) {
    return $this->get_text()->format($options);
  }
  /**
   * Format as plain text
   */
  public function format_plain($options = array()) {
    return $this->get_text()->format_plain($options);
  }
  /**
   * Format as html
   */
  public function format_html($options = array()) {
    return $this->get_text()->format_html($options);
  }
  /**
   * Form element for editing simple string
   */
  function form_element() {
    return array(
      '#default_value' => $this->get_value(array('token' => FALSE)),
      '#type' => 'textarea',
      '#rows' => 2,
    );
  }

  /**
   * Get string id / create one if not available.
   */
  function get_id() {
    if (!isset($this->sid)) {
      $this->save_string();
    }
    return $this->sid;
  }
  /**
   * Get string name
   */
  function get_name() {
    return $this->name;
  }
  /**
   * Get string title from module defaults.
   */
  function get_title() {
    return isset($this->title) ? $this->title : t('String');
  }
  /**
   * Get string title from module defaults.
   */
  function get_description() {
    return isset($this->description) ? $this->description : t('String @name', array('@name' => $this->get_name()));
  }
  /**
   * Get string type
   */
  function get_type() {
    return $this->type;
  }
  /**
   * Get string type information
   */
  function get_info($property = NULL, $default = NULL) {
    return string_type_info($this->get_type(), $property, $default);
  }
  /**
   * Get string text object.
   */
  protected function get_text() {
    return $this->get_translation(string_langcode());
  }
  /**
   * Get default string
   */
  public function get_default($langcode = NULL) {
    if (isset($this->default)) {
      return $this->default;
    }
    else {
      return new String_Not_Found(array('language' => $langcode), $this);
    }
  }

  /**
   * Get translation by language code or all if no language
   */
  public function get_translation($langcode = NULL, $fallback = TRUE) {
    if (!isset($this->translation)) {
      $this->translation = $this->load_translations();
    }
    if ($langcode) {
      $try = $fallback ? string_langcode_try($langcode) : array($langcode);
      foreach ($try as $lang) {
        if (!empty($this->translation[$lang])) {
          return $this->translation[$lang];
        }
      }
      return $fallback ? $this->get_default() : NULL;
    }
    else {
      return $this->translation;
    }
  }
  /**
   * Delete string from database
   */
  function delete() {
    if ($this->sid) {
      $this->delete_translations();
      db_delete('string')->condition('sid', $this->sid)->execute();
      $this->module_invoke('delete');
      $this->sid = NULL;
    }
  }
  /**
   * Delete translations from database.
   */
  protected function delete_translations() {
    if ($this->sid) {
      db_delete('string_text')->condition('sid', $this->sid)->execute();
    }
    $this->translation = NULL;
  }
  /**
   * Load all translations
   */
  protected function load_translations() {
    $translation = array();
    if ($this->sid) {
      $result = db_select('string_text', 's')
        ->fields('s')
        ->condition('s.sid', $this->get_id())
        ->execute();
      $class = $this->get_info('class', 'String_Translation');
      foreach ($result->fetchAllAssoc('language') as $langcode => $translation) {
        $translations[$langcode] = new $class($translation, $this);
      }
    }
    return $translations;
  }

  /**
   * Set translation object
   *
   * @param $langcode
   *   Language code.
   * @param $text
   *   Text array or string object, FALSE to set to none.
   */
  function set_translation($langcode, $text) {
    if ($text) {
      $translation = is_object($text) ? $text : $this->build_string($text);
      $translation->language = $langcode;
      $translation->set_string($this);
      $this->translation[$langcode] = $translation;
    }
    else {
      $this->translation[$langcode] = FALSE;
    }
    return $this;
  }

  /**
   * Set default string.
   *
   * @param $text
   *   Text array or string object, FALSE to set to none.
   */
  function set_default($text) {
    $this->default = is_object($text) ? $text : $this->build_string($text);
    return $this;
  }

  /**
   * Save string into static cache.
   */
  function set_cache() {
    _string_cache($this->get_name(), $this);
    return $this;
  }

  /**
   * Save to database
   */
  function save() {
    $this
      ->save_base()
      ->save_translations();
    return $this;
  }

  /**
   * Save to database
   */
  protected function save_base() {
    if ($this->sid) {
      $op = 'update';
      drupal_write_record('string', $this, 'sid');
    }
    else {
      $op = 'insert';
      drupal_write_record('string', $this);
    }
    $this->module_invoke($op);
    return $this;
  }

  /**
   * Save translations to db
   */
  protected function save_translations() {
    if (isset($this->translation)) {
      foreach ($this->translation as $langcode => $translation) {
        if ($translation) {
          $translation->sid = $this->get_id();
          $translation->save();
        }
        else {
          db_delete('string_text')
            ->condition('sid', $this->get_id())
            ->condition('language', $langcode)
            ->execute();
        }
      }
    }
    return $this;
  }
}

/**
 * String text (translation), has a value and a language
 */
class String_Translation extends String_Object {
  // String unique id
  public $sid = NULL;
  // Language code
  public $language = LANGUAGE_NONE;
  // The string text itself
  public $value = '';
  // String format
  public $format = '';
  // Date last updated
  public $changed = 0;
  // Mark for default text
  public $default = 0;
  // String base object (metadata)
  protected $string = NULL;

  /**
   * Standard constructor
   */
  public function __construct($data = NULL, $string = NULL) {
    parent::__construct($data);
    $this->string = $string;
  }
  /**
   * Delete string translation from database
   */
  protected function delete() {
    if ($this->sid) {
      db_delete('string_text')
        ->condition('sid', $this->sid)
        ->condition('language', $this->language)
        ->execute();
      $this->sid = NULL;
    }
  }
  /**
   * Form element for editing simple string
   */
  function form_element() {
    return array(
      '#default_value' => $this->get_value(array('token' => FALSE)),
      '#type' => 'textarea',
      '#rows' => 2,
    );
  }
  /**
   * Get and format string applying format and filter.
   */
  function format($options = array()) {
    return $this->format_plain($options);
  }
  /**
   * Format as plain text
   */
  public function format_plain($options = array()) {
    return check_plain($this->get_value($options));
  }
  /**
   * Format as html
   */
  public function format_html($options = array()) {
    return $this->format_plain($options);
  }
  /**
   * Get string id / create one if not available.
   */
  function get_id() {
    if (!isset($this->sid)) {
      $this->sid = $this->get_string()->get_id();
    }
    return $this->sid;
  }
  /**
   * Get language object.
   */
  function get_language() {
    $list = language_list();
    return !empty($this->language) && isset($list[$this->language]) ? $list[$this->language] : language_default();
  }
  /**
   * Get string name
   */
  function get_name() {
    return $this->get_string()->name;
  }
  /**
   * Get base string object
   */
  protected function get_string() {
    // Note base objects may be chained.
    return $this->string;
  }
  /**
   * Get string title from module defaults.
   */
  function get_title() {
    return $this->get_string()->get_title();
  }
  /**
   * Get string type
   */
  function get_type() {
    return $this->get_string()->type;
  }
  /**
   * Get raw text, may need token replacement.
   */
  function get_value($options = array()) {
    $value = $this->value;
    $options += array('token' => $this->get_string()->token);
    if ($options['token']) {
      $options += array('token data' => array(), 'token options' => array());
      $options['token options'] += array('language' => $this->get_language());
      return token_replace($this->value, $options['token data'], $options['token options']);
    }
    else {
      return $this->value;
    }
  }
  /**
   * Get translation by language code or all if no language
   */
  public function get_translation($langcode = NULL, $fallback = TRUE) {
    return $this->get_string()->get_translation($langcode, $fallback);
  }

  /**
   * Set base /parent string object
   */
  public function set_string($string) {
    $this->string = $string;
    return $this;
  }
  /**
   * Save to database
   */
  public function save() {
    return db_merge('string_text')
      ->key(array('sid' => $this->get_id(), 'language' => $this->language))
      ->fields(array('value' => $this->value, 'format' => $this->format, 'changed' => REQUEST_TIME))
      ->execute();
  }
}

/**
 * Simple string with no special filtering
 */
class String_Simple extends String_Translation {
  /**
   * Default tormat as plain string
   */
  public function format($options = array()) {
    return $this->format_plain($options);
  }
  /**
   * Format as HTML text
   */
  public function format_html($options = array()) {
    return $this->format_plain($options);
  }
}

/**
 * String text with language and format
 */
class String_Text extends String_Translation {
  /**
   * Get string text format.
   */
  public function get_format() {
    return $this->format ? $this->format : variable_get('string_default_format', 'string_text');
  }
  /**
   * Format the resulting translation or the default string applying callbacks
    */
  public function format($options = array()) {
    return $this->format_html($options);
  }

  /**
   * Apply text format
   */
  public function format_html($options = array()) {
    return check_markup($this->get_value($options), $this->get_format(), $this->language);
  }

  /**
   * Form element for editing string with text format
   */
  function form_element() {
    return array(
      '#default_value' => $this->value,
      '#type' => 'text_format',
      '#base_type' => 'textarea',
      '#format' => $this->get_format(),
    );
    return $element;
  }
}

/**
 * String with Filter XSS
 */
class String_Filter extends String_Translation {
  /**
   * Format as HTML.
   */
  function format_html($options = array()) {
    $allowed_tags = $this->get_allowed_tags();
    $string = $this->get_value($options);
    return $allowed_tags ? filter_xss($string, $allowed_tags) : filter_xss_admin($string);
  }
  /**
   * Form element for editing string with tags filter
   */
  function form_element() {
    return array(
      '#default_value' => $this->value,
      '#type' => 'textarea',
      '#rows' => 2,
      '#description' => t('Allowed HTML tags: !tags', array('!tags' => implode(', ', $this->get_allowed_tags()))),
    );
  }
  /**
   * Get allowed HTML tags
   */
  function get_allowed_tags() {
    return $this->get_string()->get_info('allowed_tags');
  }
}

/**
 * String not found
 */
class String_Not_Found extends String_Translation {
  function get_value($options = array()) {
    return t('STRING NOT FOUND: %name', array('%name' => $this->get_name()), array('langcode' => $this->language));;
  }
}