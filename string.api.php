<?php

/**
 * @file
 * Hooks provided by the String API module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define string types.
 *
 * @see string_string_type_info()
 */
function hook_string_type_info() {

}

/**
 * Alter string types.
 */
function hook_string_type_info_alter(&$types) {

}

/**
 * @} End of "addtogroup hooks".
 */