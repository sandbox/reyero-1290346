<?php
/**
 * @file
 * String form functions.
 */

/**
 * String information form
 */
function string_form_info($form, &$form_state, $string) {
  $form = string_form_element_info($string);
  $rows = array();
  foreach (language_list() as $language) {
    if ($translation = $string->get_translation($language->language, FALSE)) {
      $rows[$language->language] = array(
        check_plain($language->name),
        $translation->format_plain(),
      );
    }
  }
  $form['translations'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
  );
  return $form;
}

/**
 * String form.
 */
function string_edit_form($form, &$form_state, $string) {
  $form = string_form_element_info($string);
  // String status
  if (isset($string->status)) {
    $form['status']['status'] = array(
      '#title' => t('Enabled'),
      '#type' => 'checkbox',
      '#default_value' => $string->status,
    );
  }
  if (isset($string->token)) {
    $form['status']['token'] = array(
      '#title' => t('Token replacement'),
      '#type' => 'checkbox',
      '#default_value' => $string->token,
    );
  }
  if (!empty($form['status'])) {
    $form['status'] += array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Status'),
    );
  }
  $form['control'] = array('#type' => 'fieldset');
  $form['control']['save'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['control']['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
  return $form;
}

/**
 * Product string info form elements
 */
function string_form_element_info($string) {
  $form['string'] = array('#type' => 'value', '#value' => $string);
  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => $string->get_title(),
  );
  $form['info']['name'] = array(
    '#type' => 'item',
    '#title' => t('Name'),
    '#markup' => $string->get_name(),
  );
  $form['info']['type'] = array(
    '#type' => 'item',
    '#title' => t('Type'),
    '#markup' => $string->get_info('title'),
  );
  return $form;
}

/**
 * String edit form submit.
 */
function string_edit_form_submit($form, $form_state) {
  $string = $form_state['values']['string'];
  switch ($form_state['values']['op']) {
    case t('Save'):
      foreach ($form_state['values']['status'] as $key => $value) {
        $string->$key = $value;
      }
      $string->save();
      drupal_set_message(t('The string has been updated.'));
      break;
    case t('Delete'):
      $string->delete();
      drupal_set_message(t('The string has been deleted'));
      break;
  }
}

/**
 * String translate form.
 */
function string_translate_form($form, &$form_state, $string) {
  $form = string_form_element_info($string);

  $form['translation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Translations'),
    '#tree' => TRUE,
  );
  foreach (language_list() as $lang => $language) {
    $translation = $string->get_translation($lang);
    $form['translation'][$lang] = $translation->form_element();
    $form['translation'][$lang]['#title'] = $language->name;
  }
  $form['control'] = array('#type' => 'fieldset');
  $form['control']['save'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['control']['reset'] = array('#type' => 'submit', '#value' => t('Reset to defaults'));
  return $form;
}

/**
 * String translate form.
 */
function string_translate_form_submit($form, $form_state) {
  $string = $form_state['values']['string'];
  switch ($form_state['values']['op']) {
    case t('Save'):
      foreach ($form_state['values']['translation'] as $langcode => $translation) {
        // It may be just a string, or value and format
        if (is_array($translation)) {
          $text = $translation['value'];
          $format = $translation['format'];
        }
        else {
          $text = $translation;
          $format = NULL;
        }
        if ($text) {
          $string->set_translation($langcode, $text, array('format' => $format));
        }
        else {
          $string->set_translation($langcode, FALSE);
        }
      }
      $string->override = 1;
      $string->save();
      drupal_set_message(t('Your translations have been updated.'));
      break;
    case t('Reset to defaults'):
      module_load_include('build.inc', 'string');
      $string->delete_translations();
      foreach (string_build_module_translation($string->name) as $langcode => $text) {
        $string->set_translation($langcode, $text);
      }
      $string->save_translations();
      drupal_set_message(t('String translations have been reset to default values.'));
      break;
  }
}

/**
 * Format list
 */
function string_format_list($allowed) {
  $list = array();
  foreach ($allowed as $format) {
    $list[$format] = string_format_name($format);
  }
  // If allowed text formats, add all of them.
  if (in_array(STRING_FORMAT_MARKUP, $allowed, TRUE)) {
    foreach (filter_formats() as $key => $format) {
      $list[$key] = t('Text format: @name', array('@name' => $format->name));
    }
  }
  return $list;
}

