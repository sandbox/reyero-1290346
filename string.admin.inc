<?php 
/**
 * @file
 * String administration
 */

/**
 * String overview, plain list
 */
function string_custom_overview($module = NULL) {
   $query = db_select('string', 's')
     ->fields('s', array('sid'))
     ->condition('s.parent', '');
   if ($module) {
     $query->condition('s.module', $module);
   }
   if ($sids = $query->execute()->fetchCol()) {
     $list = string_load_multiple(array('sid' => $sids));
     foreach ($list as $string) {
       if ($class = string_type_info($string->type, 'class')) {
         $rows[] = array(
           l($string->name, STRING_MENU_PATH . '/string/' . $string->name),
           string_type_info($string->type, 'title'),
           $string->get_title(),
         );
       }
       else {
         $rows[] = array(
           l($string->get_title(), STRING_MENU_PATH . '/string/' . $string->name),
           $string->type,
           t('Unknown'),
         );
       }
     }
   }
   if (!empty($rows)) {
     return theme('table', array(
       'header' => array(t('Name'), t('Type'), t('Value')),
       'rows' => $rows));
   }
   else {
     return t('No strings found.');
   }
}

/**
 * String info subform
 */
function string_custom_string_subform($string) {
  $form['string'] = array('#type' => 'value', '#value' => $string);
  $form['info'] = array(
    '#type' => 'fieldset',
    '#title' => string_type_info($string->type, 'title', t('String')),
  );
  $form['info']['name'] = array(
    '#type' => 'item',
    '#title' => t('Name'),
    '#markup' => $string->name,
  );
  $form['info']['value'] = array(
    '#type' => 'item',
    '#title' => t('Value'),
    '#markup' => $string->format_html(),
  );
  return $form;
}
